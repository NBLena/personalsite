# Write me! Find My Stuff!

Email: [lena@techromancer.club](mailto:lena@techromancer.club)

YouTube: [Gescheit Gespielt](https://youtube.com/gescheitgespielt)

Mastodon: [cybre.space](https://cybre.space/@nblena)

Itch.io: [Project List](https://nblena.itch.io)
